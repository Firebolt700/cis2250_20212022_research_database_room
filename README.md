# Database - Room #

### What is this repository for? ###

Research on implementing and using the Room library with databases in Android Studio

### How do I get set up? ###

Follow research documentation on how to set up the Room library

### Contribution ###

* Ben MacDonald
* Jennifer Whittaker