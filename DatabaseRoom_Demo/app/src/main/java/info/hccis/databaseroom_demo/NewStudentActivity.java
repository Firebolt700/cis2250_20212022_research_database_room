package info.hccis.databaseroom_demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class NewStudentActivity extends AppCompatActivity {

    // creating a variables for our button and edittext.
    private EditText studentFirstNameEdt, studentLastNameEdt, studentYearEdt;
    private Button studentBtn;

    // creating a constant string variable for our
    // student first name, last name, and year
    public static final String EXTRA_ID = "com.gtappdevelopers.gfgroomdatabase.EXTRA_ID";
    public static final String EXTRA_STUDENT_FIRST_NAME = "com.gtappdevelopers.gfgroomdatabase.EXTRA_STUDENT_FIRST_NAME";
    public static final String EXTRA_STUDENT_LAST_NAME = "com.gtappdevelopers.gfgroomdatabase.EXTRA_STUDENT_LAST_NAME";
    public static final String EXTRA_STUDENT_YEAR = "com.gtappdevelopers.gfgroomdatabase.EXTRA_STUDENT_YEAR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_student);

        // initializing our variables for each view.
        studentFirstNameEdt = findViewById(R.id.idEdtStudentFirstName);
        studentLastNameEdt = findViewById(R.id.idEdtStudentLastName);
        studentYearEdt = findViewById(R.id.idEdtStudentYear);
        studentBtn = findViewById(R.id.idBtnSaveStudent);

        // below line is to get intent as we
        // are getting data via an intent.
        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            // if we get id for our data then we are
            // setting values to our edit text fields.
            studentFirstNameEdt.setText(intent.getStringExtra(EXTRA_STUDENT_FIRST_NAME));
            studentLastNameEdt.setText(intent.getStringExtra(EXTRA_STUDENT_LAST_NAME));
            studentYearEdt.setText(intent.getStringExtra(EXTRA_STUDENT_YEAR));
        }
        // adding on click listener for our save button.
        studentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // getting text value from edittext and validating if
                // the text fields are empty or not.
                String studentFirstName = studentFirstNameEdt.getText().toString();
                String studentLastName = studentLastNameEdt.getText().toString();
                String studentYear = studentYearEdt.getText().toString();
                if (studentFirstName.isEmpty() || studentLastName.isEmpty() || studentYear.isEmpty()) {
                    Toast.makeText(NewStudentActivity.this, "Please enter the valid student details.", Toast.LENGTH_SHORT).show();
                    return;
                }
                // calling a method to save our student.
                saveStudent(studentFirstName, studentLastName, studentYear);
            }
        });
    }

    private void saveStudent(String studentFirstName, String studentLastName, String studentYear) {
        // inside this method we are passing
        // all the data via an intent.
        Intent data = new Intent();

        // in below line we are passing all our student detail.
        data.putExtra(EXTRA_STUDENT_FIRST_NAME, studentFirstName);
        data.putExtra(EXTRA_STUDENT_LAST_NAME, studentLastName);
        data.putExtra(EXTRA_STUDENT_YEAR, studentYear);
        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            // in below line we are passing our id.
            data.putExtra(EXTRA_ID, id);
        }

        // at last we are setting result as data.
        setResult(RESULT_OK, data);

        // displaying a toast message after adding the data
        Toast.makeText(this, "Student has been saved to Room Database. ", Toast.LENGTH_SHORT).show();
    }
}