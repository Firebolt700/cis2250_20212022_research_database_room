package info.hccis.databaseroom_demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    // creating a variables for our recycler view.
    private RecyclerView studentsRV;
    private static final int ADD_STUDENT_REQUEST = 1;
    private static final int EDIT_STUDENT_REQUEST = 2;
    private ViewModal viewmodal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initializing our variable for our recycler view and fab.
        studentsRV = findViewById(R.id.idRVStudents);
        FloatingActionButton fab = findViewById(R.id.idFABAdd);

        // adding on click listener for floating action button.
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // starting a new activity for adding a new student
                // and passing a constant value in it.
                Intent intent = new Intent(MainActivity.this, NewStudentActivity.class);
                startActivityForResult(intent, ADD_STUDENT_REQUEST);
            }
        });

        // setting layout manager to our adapter class.
        studentsRV.setLayoutManager(new LinearLayoutManager(this));
        studentsRV.setHasFixedSize(true);

        // initializing adapter for recycler view.
        final StudentRVAdapter adapter = new StudentRVAdapter();

        // setting adapter class for recycler view.
        studentsRV.setAdapter(adapter);

        // passing a data from view modal.
        viewmodal = ViewModelProviders.of(this).get(ViewModal.class);

        // below line is use to get all the students from view modal.
        viewmodal.getAllStudents().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(List<Student> models) {
                // when the data is changed in our models we are
                // adding that list to our adapter class.
                adapter.submitList(models);
            }
        });
        // below method is use to add swipe to delete method for item of recycler view.
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                // on recycler view item swiped then we are deleting the item of our recycler view.
                viewmodal.delete(adapter.getStudentAt(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "Student deleted", Toast.LENGTH_SHORT).show();
            }
        }).
                // below line is use to attach this to recycler view.
                        attachToRecyclerView(studentsRV);
        // below line is use to set item click listener for our item of recycler view.
        adapter.setOnItemClickListener(new StudentRVAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Student model) {
                // after clicking on item of recycler view
                // we are opening a new activity and passing
                // a data to our activity.
                Intent intent = new Intent(MainActivity.this, NewStudentActivity.class);
                intent.putExtra(NewStudentActivity.EXTRA_ID, model.getId());
                intent.putExtra(NewStudentActivity.EXTRA_STUDENT_FIRST_NAME, model.getFirstName());
                intent.putExtra(NewStudentActivity.EXTRA_STUDENT_LAST_NAME, model.getLastName());
                intent.putExtra(NewStudentActivity.EXTRA_STUDENT_YEAR, model.getYear());

                // below line is to start a new activity and
                // adding a edit student constant.
                startActivityForResult(intent, EDIT_STUDENT_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_STUDENT_REQUEST && resultCode == RESULT_OK) {
            String studentFirstName = data.getStringExtra(NewStudentActivity.EXTRA_STUDENT_FIRST_NAME);
            String studentLastName = data.getStringExtra(NewStudentActivity.EXTRA_STUDENT_LAST_NAME);
            String studentYear = data.getStringExtra(NewStudentActivity.EXTRA_STUDENT_YEAR);
            Student model = new Student(studentFirstName, studentLastName, studentYear);
            viewmodal.insert(model);
            Toast.makeText(this, "Student saved", Toast.LENGTH_SHORT).show();
        } else if (requestCode == EDIT_STUDENT_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(NewStudentActivity.EXTRA_ID, -1);
            if (id == -1) {
                Toast.makeText(this, "Student can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }
            String studentFirstName = data.getStringExtra(NewStudentActivity.EXTRA_STUDENT_FIRST_NAME);
            String studentLastName = data.getStringExtra(NewStudentActivity.EXTRA_STUDENT_LAST_NAME);
            String studentYear = data.getStringExtra(NewStudentActivity.EXTRA_STUDENT_YEAR);
            Student model = new Student(studentFirstName, studentLastName, studentYear);
            model.setId(id);
            viewmodal.update(model);
            Toast.makeText(this, "Student updated", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Student not saved", Toast.LENGTH_SHORT).show();
        }
    }
}