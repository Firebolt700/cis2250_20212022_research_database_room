package info.hccis.databaseroom_demo;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ViewModal extends AndroidViewModel {

    // creating a new variable for Student repository.
    private StudentRepository repository;

    // below line is to create a variable for live
    // data where all the Students are present.
    private LiveData<List<Student>> allStudents;

    // constructor for our view modal.
    public ViewModal(@NonNull Application application) {
        super(application);
        repository = new StudentRepository(application);
        allStudents = repository.selectAllStudents();
    }

    // below method is use to insert the data to our repository.
    public void insert(Student model) {
        repository.insert(model);
    }

    // below line is to update data in our repository.
    public void update(Student model) {
        repository.update(model);
    }

    // below line is to delete the data in our repository.
    public void delete(Student model) {
        repository.delete(model);
    }

    // below method is to get all the Students in our list.
    public LiveData<List<Student>> getAllStudents() {
        return allStudents;
    }
}
