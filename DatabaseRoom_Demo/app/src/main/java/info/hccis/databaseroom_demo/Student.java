package info.hccis.databaseroom_demo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

// Sets the name of the table in the database
@Entity(tableName = "Student")
public class Student {

    // table columns
    @PrimaryKey(autoGenerate = true) // Auto increments ID column
    private int id;
    private String firstName;
    private String lastName;
    private String year;

    // Constructor
    public Student(String firstName, String lastName, String year) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.year = year;
    }

    // Getters and setters
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
