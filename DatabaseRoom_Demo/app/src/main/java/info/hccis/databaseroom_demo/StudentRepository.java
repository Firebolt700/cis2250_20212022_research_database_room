package info.hccis.databaseroom_demo;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class StudentRepository {

    // below line is the create a variable
    // for dao and list for all Students.
    private DAO dao;
    private LiveData<List<Student>> allStudents;

    // creating a constructor for our variables
    // and passing the variables to it.
    public StudentRepository(Application application) {
        StudentDatabase database = StudentDatabase.getInstance(application);
        dao = database.Dao();
        allStudents = dao.selectAllStudents();
    }

    // creating a method to insert the data to our database.
    public void insert(Student model) {
        new InsertStudentAsyncTask(dao).execute(model);
    }

    // creating a method to update data in database.
    public void update(Student model) {
        new UpdateStudentAsyncTask(dao).execute(model);
    }

    // creating a method to delete the data in our database.
    public void delete(Student model) {
        new DeleteStudentAsyncTask(dao).execute(model);
    }

    // below method is to read all the Students.
    public LiveData<List<Student>> selectAllStudents() {
        return allStudents;
    }

    // we are creating a async task method to insert new Student.
    private static class InsertStudentAsyncTask extends AsyncTask<Student, Void, Void> {
        private DAO dao;

        private InsertStudentAsyncTask(DAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Student... model) {
            // below line is use to insert our modal in dao.
            dao.insert(model[0]);
            return null;
        }
    }

    // we are creating a async task method to update our Student.
    private static class UpdateStudentAsyncTask extends AsyncTask<Student, Void, Void> {
        private DAO dao;

        private UpdateStudentAsyncTask(DAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Student... models) {
            // below line is use to update
            // our modal in dao.
            dao.update(models[0]);
            return null;
        }
    }

    // we are creating a async task method to delete Student.
    private static class DeleteStudentAsyncTask extends AsyncTask<Student, Void, Void> {
        private DAO dao;

        private DeleteStudentAsyncTask(DAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Student... models) {
            // below line is use to delete
            // our Student modal in dao.
            dao.delete(models[0]);
            return null;
        }
    }

}
