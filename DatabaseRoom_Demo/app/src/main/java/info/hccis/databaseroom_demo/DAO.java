package info.hccis.databaseroom_demo;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;

@androidx.room.Dao
public interface DAO {

    @Insert
    void insert(Student model);

    @Update
    void update(Student model);

    @Delete
    void delete(Student model);

    @Query("SELECT * FROM Student ORDER BY id ASC")
    LiveData<List<Student>> selectAllStudents();

}
